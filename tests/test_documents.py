import os
from eorg.parser import parse
from eorg.generate import html


def test_basic():
    with open(os.path.abspath("./tests/fixtures/test.org"), "r") as fp:
        doc = parse(fp)
        assert doc.title != ""
        assert doc.author != ""
        assert len(doc) == 20


def test_body():
    with open(os.path.abspath("./tests/fixtures/test.org"), "r") as fp:
        doc = parse(fp)
        assert len([i for i in doc.body()]) > 0


def test_html_output():
    with open(os.path.abspath("./tests/fixtures/test.org"), "r") as fp:
        doc = parse(fp)
        htmlbody = html(doc).read()
        print(htmlbody)
        assert (
            htmlbody
            == """<div class="org-body"><h2> Header 1</h2>
<h3> Sub Header 1</h3>
<p class="flow-text">body <code>text</code>
 over multiple <b>lines</b>
 </p><h3> Sub Header 2</h3>
<h2> Header 2</h2>
<table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1</pre></div></td><td class="code"><div class="highlight"><pre><span></span><span class="p">(</span><span class="nv">some</span> <span class="nv">lispy</span> <span class="nv">code</span><span class="p">)</span>
</pre></div>
</td></tr></table><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1</pre></div></td><td class="code"><div class="highlight"><pre><span></span><span class="p">(</span><span class="nv">test</span> <span class="nv">code</span><span class="p">)</span>
</pre></div>
</td></tr></table></div>"""
        )
