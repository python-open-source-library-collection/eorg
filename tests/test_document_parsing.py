import os
import pytest
from io import StringIO
from eorg import tokens
from eorg.tokens import Token
from eorg.parser import parse
from eorg.parser import parse_text


def test_emphasis():
    text = "parse emphasis *bold text* _underlined text_ /italic text/ normal text"
    expected = [
        Token(token=tokens.TEXT, value="parse emphasis "),
        Token(token=tokens.BOLD, value="bold text"),
        Token(token=tokens.TEXT, value=" "),
        Token(token=tokens.UNDERLINED, value="underlined text"),
        Token(token=tokens.TEXT, value=" "),
        Token(token=tokens.ITALIC, value="italic text"),
        Token(tokens.TEXT, " normal text"),
    ]
    result = parse_text(text)
    assert result[0].token == tokens.TEXT
    assert expected[0].value == result[0].value
    assert result[1].token == tokens.BOLD
    assert expected[1].value == result[1].value
    assert result[2].token == tokens.TEXT
    assert expected[2].value == result[2].value
    assert result[3].token == tokens.UNDERLINED
    assert expected[3].value == result[3].value
    assert result[4].token == tokens.TEXT
    assert expected[4].value == result[4].value
    assert result[5].token == tokens.ITALIC
    assert expected[5].value == result[5].value
    assert result[6].token == tokens.TEXT
    assert expected[6].value == result[6].value


def test_image():
    text = "parse image [[../../test.jpg][test]] after image"
    expected = [
        Token(tokens.TEXT, "parse image "),
        Token(tokens.IMAGE, ["../../test.jpg", "test"]),
        Token(tokens.TEXT, " after image"),
    ]
    result = parse_text(text)
    assert result[0].value == expected[0].value
    assert result[1].value == expected[1].value
    assert result[2].value == expected[2].value

    text = StringIO("[[../../../images/opengl/point-sprite-shader.png]]")
    expected = [
        Token(
            tokens.IMAGE,
            ["../../../images/opengl/point-sprite-shader.png", ""],
        )
    ]
    result = parse(text).doc
    assert result[0].value == expected[0].value


def test_image_with_caption():
    text = StringIO(
        """#+CAPTION: Test Image
[[../../test.jpg]]"""
    )
    expected = [
        Token(
            tokens.IMAGE,
            ["../../test.jpg", ""],
            attrs={"caption": " Test Image"},
        )
    ]
    result = parse(text).doc
    assert len(result) == 1
    assert result[0].token == expected[0].token
    assert result[0].value == expected[0].value
    assert result[0].attrs == expected[0].attrs

    text = StringIO(
        """#+CAPTION: Test Image
[[../../test.jpg][test]]"""
    )
    expected = [
        Token(
            tokens.IMAGE,
            ["../../test.jpg", "test"],
            attrs={"caption": " Test Image"},
        )
    ]
    result = parse(text).doc
    assert len(result) == 1
    assert result[0].token == expected[0].token
    assert result[0].value == expected[0].value
    assert result[0].attrs == expected[0].attrs


def test_multiple_images():
    text = StringIO(
        """[[./images.jpg]]
[[./images.jpg][test]]"""
    )
    expected = [
        Token(tokens.IMAGE, ["./images.jpg", ""]),
        Token(tokens.IMAGE, ["./images.jpg", "test"]),
    ]
    result = parse(text).doc
    assert len(result) == 2
    assert result[0].token == expected[0].token
    assert result[0].value == expected[0].value
    assert result[1].token == expected[1].token
    assert result[1].value == expected[1].value


def test_link():
    text = "parse link [[../../test.html][test]] after link"
    expected = [
        Token(tokens.TEXT, "parse link "),
        Token(tokens.LINK, ["../../test.html", "test"]),
        Token(tokens.TEXT, " after link"),
    ]
    result = parse_text(text)
    assert result[0].value == expected[0].value
    assert result[1].value == expected[1].value
    assert result[2].value == expected[2].value


def test_example():
    text = StringIO(
        """
#+BEGIN_EXAMPLE
*I'm bold text*
/I'm italic text/
_I'm underlined text_
#+END_EXAMPLE"""
    )

    expected = [
        Token(tokens.BLANK, ""),
        Token(
            tokens.EXAMPLE,
            """*I'm bold text*
/I'm italic text/
_I'm underlined text_
""",
        ),
    ]
    result = parse(text).doc
    assert result[0].value == expected[0].value
    assert result[1].value == expected[1].value


def test_src_block():
    text = StringIO(
        """
#+BEGIN_SRC sh :results output drawer
head -n 5 examples/html-plain/example.py
#+END_SRC"""
    )

    expected = [
        Token(tokens.BLANK, ""),
        Token(tokens.SOURCE, """head -n 5 examples/html-plain/example.py\n"""),
    ]
    result = parse(text).doc
    assert result[0].token == tokens.BLANK
    assert result[0].value == expected[0].value
    assert result[1].attrs.get("language") == "sh"
    assert result[1].value == expected[1].value


def test_bullet_block():
    text = StringIO(
        """
+ Bullet 1
+ Bullet 2"""
    )

    expected = [
        Token(tokens.BLANK, ""),
        Token(tokens.BULLET, """+ Bullet 1\n+ Bullet 2\n"""),
    ]
    result = parse(text).doc
    assert result[0].token == tokens.BLANK
    assert result[0].value == expected[0].value
    assert result[1].token == tokens.BULLET
    assert result[1].value == expected[1].value

    text = StringIO(
        """
1. Bullet 1
2. Bullet 2"""
    )

    expected = [
        Token(tokens.BLANK, ""),
        Token(tokens.BULLET, """1. Bullet 1\n2. Bullet 2\n"""),
    ]
    result = parse(text).doc
    assert result[0].token == tokens.BLANK
    assert result[0].value == expected[0].value
    assert result[1].token == tokens.BULLET
    assert result[1].value == expected[1].value

    text = StringIO(
        """
- Bullet 1
- Bullet 2
 -Bullet 3"""
    )

    expected = [
        Token(tokens.BLANK, ""),
        Token(tokens.BULLET, """- Bullet 1\n- Bullet 2\n -Bullet 3\n"""),
    ]
    result = parse(text).doc
    assert result[0].token == tokens.BLANK
    assert result[0].value == expected[0].value
    assert result[1].token == tokens.BULLET
    assert result[1].value == expected[1].value


@pytest.mark.skip
def test_src_block_images():
    text = StringIO(
        """
#+BEGIN_SRC latex :exports results :file test.png :results raw file
\begin{equation}
x=\sqrt{b}
\end{equation}
#+END_SRC

#+RESULTS:
[[file:test.png]]
"""
    )
    expected = [
        Token(tokens.BLANK, ""),
        Token(
            tokens.SOURCE, """\begin{equation}\nx=\sqrt{b}\n\end{equation}"""
        ),
        Token(tokens.BLANK, ""),
    ]
    result = parse(text).doc
    assert result[0].token == tokens.BLANK
    assert result[0].value == expected[0].value
    assert result[1].token == tokens.SOURCE
    assert result[2].token == tokens.BLANK
    assert result[3].value == expected[0].value
    assert result[3].token == tokens.RESULTS
