import os
import pytest
from io import StringIO
from eorg.parser import parse
from eorg.parser import Token


CODE_BLOCK_EXAMPLE_01 = StringIO(
    """
#+BEGIN_SRC shell :results silent :tangle .env
elcato create --path=/tmp/myblog
#+END_SRC"""
)


def test_block_settings():
    expected = [
        Token(token="BREAK", value=""),
        Token(
            token="SRC_BEGIN",
            value="elcato create --path=/tmp/myblog\n",
            attrs={
                "language": "shell",
                "results": ["silent"],
                "tangle": [".env"],
            },
        ),
    ]

    result = [i for i in parse(CODE_BLOCK_EXAMPLE_01)]
    assert result[0].value == expected[0].value
    assert result[1].value == expected[1].value
    assert result[1].attrs == expected[1].attrs
    #assert result == expected


def test_export_settings():
    document = StringIO("""
    #+TITLE: Tests
#+BEGIN_SRC shell :exports results
elcato create --path=/tmp/myblog
#+END_SRC

#+RESULTS:
result block here
    """)
    result = parse(document).doc
    assert result[2].attrs.get('exports') == ['results']
