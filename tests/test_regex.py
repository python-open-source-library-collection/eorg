import os
import re
import pytest
from io import StringIO
from eorg import const
from eorg.parser import parse
from eorg.generate import html
from eorg.helper import parse_img_or_link


def test_meta_headers():
    text = "#+TITLE: test title"
    rx = const.t_META
    match = re.search(rx, text)
    assert match is not None

    text = "#+UNKNOWN: test title"
    rx = const.t_META
    match = re.search(rx, text)
    assert match is None

    text = "#+UNKNOWN: test title"
    rx = const.t_META_OTHER
    match = re.search(rx, text)
    assert match is not None


def test_example():
    text = "#+BEGIN_EXAMPLE"
    rx = const.t_EXAMPLE_BEGIN
    match = re.search(rx, text)
    assert match is not None

    text = "#+BEGIN_EXAMPLE "
    rx = const.t_EXAMPLE_BEGIN
    match = re.search(rx, text)
    assert match is not None


def test_source():
    # invalid if no language specified
    text = "#+BEGIN_SRC"
    rx = const.t_SRC_BEGIN
    match = re.search(rx, text)
    assert match is None

    text = "#+BEGIN_SRC "
    rx = const.t_SRC_BEGIN
    match = re.search(rx, text)
    assert match is not None

    text = "#+BEGIN_SRC sh :results silent"
    rx = const.t_SRC_BEGIN
    match = re.search(rx, text)
    assert match is not None


def test_results():
    text = "#+RESULTS:"
    rx = const.t_RESULTS_START
    match = re.search(rx, text)
    assert match is not None


def test_bullets():
    # invalid if no language specified
    text = " + bullet 1"
    rx = const.t_BULLET_START
    match = re.search(rx, text)
    assert match is not None

    text = "+ bullet 1"
    rx = const.t_BULLET_START
    match = re.search(rx, text)
    assert match is not None


def test_captions_regex():
    text = "#+CAPTION: Test"
    rx = const.t_CAPTIONS
    match = re.search(rx, text)
    assert match is not None

    text = "#+CAPTION:Test"
    rx = const.t_CAPTIONS
    match = re.search(rx, text)
    assert match is not None


def test_image_regex():
    token = const.TOKENS[const.tokens.IMAGE]
    text = "[[../../image.jpg]]"
    match = re.search(token.start, text)
    assert match is not None
    block, token = parse_img_or_link(text[0], iter(text[1:]))
    assert token.value[0] == "../../image.jpg"
    assert token.value[1] == ""

    token = const.TOKENS[const.tokens.IMAGE]
    text = "[[../../image.jpg][test]]"
    match = re.search(token.start, text)
    assert match is not None

    block, token = parse_img_or_link(text[0], iter(text[1:]))
    assert token.value[0] == "../../image.jpg"
    assert token.value[1] == "test"
