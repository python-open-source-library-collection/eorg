import os
import pytest
from io import StringIO
from eorg import tokens
from eorg.parser import parse
from eorg.generate import html


def test_table():
    document = """| Header 1 | Header 2 |
| row 1 | row 2 |
    """
    doc = parse(StringIO(document))
    assert doc.doc[0].token == tokens.TABLE
    r1 = doc.doc[0].value.strip()
    r2 = document.strip()
    assert r1 == r2


def test_example():
    document = """#+BEGIN_EXAMPLE
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="view-source:https://edwardtufte.github.io/tufte-css/tufte.css" />
#+END_EXAMPLE"""
    doc = parse(StringIO(document))
    assert doc.doc[0].token == tokens.EXAMPLE
    r1 = doc.doc[0].value.strip()
    r2 = document.strip()
    r2 = '#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="view-source:https://edwardtufte.github.io/tufte-css/tufte.css" />'
    assert r1 == r2
