import os
import pytest
from eorg import tokens
from eorg.tokens import Token
from eorg.parser import parse


def test_fetch_attribute():
    with open(os.path.abspath("./tests/fixtures/test.org"), "r") as fp:
        doc = parse(fp)
        assert doc.title == "     Emacs org-mode examples"


def test_fetch_non_existant_attribute():
    with open(os.path.abspath("./tests/fixtures/test.org"), "r") as fp:
        doc = parse(fp)
        with pytest.raises(AttributeError):
            doc.fake


def test_fetch_image_list():
    with open(os.path.abspath("./tests/fixtures/test_images.org"), "r") as fp:
        doc = parse(fp)
        expected = [
            Token(tokens.IMAGE, ["./images.jpg", ""]),
            Token(tokens.IMAGE, ["./images.jpg", "test"]),
        ]
        images = [i for i in doc.images()]
        assert len(images) == 2
