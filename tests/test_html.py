import snippets
from io import StringIO
from eorg import tokens
from eorg.tokens import Token
from eorg.parser import parse
from eorg.generate import html


def test_bullet_block():
    expected = """<div class="org-body"><ul class="browser-default"><li class="collection-item">Bullet 1</li><li class="collection-item">Bullet 2</li></ul></div>"""
    result = html(parse(snippets.bullet_plus_snippet).doc)
    assert result.read() == expected


def test_render_results():
    text = StringIO(
        """
#+RESULTS:
[[file:test.png]]
"""
    )
    expected = [
        Token(tokens.IMAGE, ['test.png', ''], attrs=None),
        Token(tokens.TEXT, "\n"),
    ]
    doc = parse(text)
    tags = doc.doc
    assert tags[0].token == tokens.BLANK
    assert len(tags[1].value) == len(expected)
    assert tags[1].token == tokens.RESULTS

    assert tags[1].value[0].value == expected[0].value
    assert tags[1].value[1].value == expected[1].value

    htmlbody = html(tags).read()
    assert htmlbody == '<div class="org-body"><img style="margin:auto;" src="test.png" alt="" /></div>'
