from io import StringIO

bullet_plus_snippet = StringIO("""
+ Bullet 1
+ Bullet 2""")


bullet_number_snippet = StringIO("""
1. Bullet 1
2. Bullet 2""")
