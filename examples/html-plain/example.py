import os
from eorg.parser import parse
from eorg.generate import html

with open(os.path.abspath('../../tests/fixtures/test.org'), 'r') as fp:
    doc = parse(fp)
    with open('test.html', 'w') as fp:
        fp.write(html(doc).read())
