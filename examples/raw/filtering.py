import os
from eorg.parser import parse
from eorg.generate import html

with open(os.path.abspath('../../tests/fixtures/test.org'), 'r') as fp:
    doc = parse(fp)
    for row in doc.filter('SRC_BEGIN'):
        print(row.token)
        print(row.value)
