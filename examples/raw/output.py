import os
import pytest
from eorg.parser import parse


with open(os.path.abspath("../../tests/fixtures/test.org"), "r") as fp:
    doc = parse(fp)
    print(doc.title)
    print(doc.keywords)
    for item in doc:
        print(item)
