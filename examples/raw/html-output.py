import os
from eorg.parser import parse
from eorg.html import generate

doc = []
with open(os.path.abspath("../../tests/fixtures/test.org"), "r") as fp:
    doc = parse(fp)

print(generate(doc).read())
