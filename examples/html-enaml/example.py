import os
import enaml
from eorg.parser import parse
from web.core.app import WebApplication

doc = []
with open(os.path.abspath("../../tests/fixtures/test.org"), "r") as fp:
    doc = parse(fp)

app = WebApplication()
with enaml.imports():
    from index import Index

view = Index()

with open("output.html", "wb") as f:
    f.write(view.render(card=doc))
